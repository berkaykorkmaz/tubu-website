var express = require('express'),
  path = require('path'),
  app = express();

//set the port
app.set('port', 8000);

//tell express that we want to use the www folder
//for our static assets
app.use(express.static(path.join(__dirname, 'www')));

//show 404 page if the requested page doesn't exist
app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'www', '404.html'));
});


// Listen for requests
var server = app.listen(app.get('port'), function () {
  console.log('The server is running on http://localhost:' + app.get('port'));
});