$(document).ready(function () {

  // Collapse navbar on click (mobile)
  $('.nav-link').on('click', function () {
    $('.navbar-collapse').collapse('hide');
  });

  // Add smooth scrolling to all links
  $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();

    var target = this.hash,
      $target = $(target);

    $('html').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  // Print copyright with current year
  $('#copyright').html('© ' + new Date().getFullYear() + ' Powered by TUBU');

  // Give current domain as href to logo
  $('a.navbar-brand').attr("href", window.location.origin);

  // Add shadow to navbar on scroll
  $(document).scroll(function () {
    $('#navbar').toggleClass('navbar-shadow', $(document).scrollTop() >= 60);
  });

});